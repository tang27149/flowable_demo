package com.example.flowable.holiday.delegate;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * @author Foolish
 * @description
 * @date: 2018/9/21 14:46
 */
public class CallExternalSystemDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("为员工调用外部系统：" + execution.getVariable("employee"));
    }
}
